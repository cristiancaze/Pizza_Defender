using System.Collections.Generic;
using UnityEngine;


public class TowerBase : MonoBehaviour, ITickable, IAttackable
{
    [SerializeField] protected TowerStats stats;
    [Tooltip("What kind of enemie am I")]
    [SerializeField] private EnemyType enemyType;
    [SerializeField] protected List<EnemyType> targetsToAttack;
    protected float health;
    protected AmmoPool ammoPool;
    protected int atackSpeedTickCounter;
    protected int specialAtackTickCounter;
    protected bool enemiesInRange;
    protected int maxDistanceRaycast = 10;
    private RaycastHit hit;

    protected void Awake()
    {
        health = stats.MaxHealth;
        ammoPool = GetComponentInChildren<AmmoPool>();
    }

    protected virtual void Atack()
    {
        AmmoProjectile ammoprojectile = ammoPool.GetAmmo();
        ammoprojectile.Initialize(stats.AtackAmount, false, targetsToAttack);
    }

    protected virtual void SpecialAtack()
    {
        
    }

    public virtual void OnTick()
    {
        atackSpeedTickCounter += 1;
        specialAtackTickCounter += 1;

        if (atackSpeedTickCounter > stats.AtacksSpeedInverval)
        {
            if (CheckForEnemiesInRange())
                Atack();
            atackSpeedTickCounter = 0;
        }

        if (specialAtackTickCounter > stats.SpecialAtackInterval)
        {
            if (CheckForEnemiesInRange())
                SpecialAtack();
            specialAtackTickCounter = 0;
        }
    }


    protected bool CheckForEnemiesInRange()
    {
        if (Physics.Raycast(transform.position, Vector3.right, out hit, maxDistanceRaycast))
        {
            if (hit.collider.TryGetComponent(out IAttackable IAttackable))
                if (targetsToAttack.Contains(IAttackable.GetEnemieType()))
                    return true;
        }
        return false;
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
    }

    public EnemyType GetEnemieType()
    {
        return enemyType;
    }
}
