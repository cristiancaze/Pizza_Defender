using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine;

public class AmmoProjectile : MonoBehaviour
{
    private float damageToDeal;
    private bool isFreezeAtack;
    private bool isActive;
    private float speed = 5f;
    private List<EnemyType> targetsToAttack;
    private Vector3 initialPos;


    //Scalable to be a specialAction scriptable object and separated prefabs so no parameters are required and easealy can add new projectile types
    public void Initialize(float _damageToDeal, bool _isFreezeAtack, List<EnemyType> _targetsToAttack)
    {
        initialPos = Vector3.up / 2;
        targetsToAttack = _targetsToAttack;
        damageToDeal = _damageToDeal;
        isFreezeAtack = _isFreezeAtack;
        isActive = true;
        transform.position = initialPos;
    }

    private void Update()
    {
        if (!isActive)
            return;
        transform.position += Vector3.right * speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out IAttackable IAttackable))
            if (targetsToAttack.Contains(IAttackable.GetEnemieType()))
                IAttackable.TakeDamage(damageToDeal);

        if (isFreezeAtack)
        {
            if (other.TryGetComponent(out IFreezeable IFreezeable))
                if (targetsToAttack.Contains(IFreezeable.GetEnemieType()))
                    IFreezeable.Freeze();
        }
    }
}
