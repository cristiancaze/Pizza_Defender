using System.Collections.Generic;
using UnityEngine;

public class AmmoPool : MonoBehaviour
{
    [SerializeField] private AmmoProjectile ammoPrefab;
    [SerializeField] private int poolSize;

    private List<AmmoProjectile> ammoList;

    private void Awake()
    {
        ammoList = new List<AmmoProjectile>();

        // Create the ammo pool, the ammo is the root scene for performance
        for (int i = 0; i < poolSize; i++)
        {
            AmmoProjectile ammo = Instantiate(ammoPrefab);
            ammo.gameObject.SetActive(false);
            ammoList.Add(ammo);
        }
    }

    public AmmoProjectile GetAmmo()
    {
        // Retrieve an available ammo from the pool
        for (int i = 0; i < ammoList.Count; i++)
        {
            if (!ammoList[i].gameObject.activeInHierarchy)
            {
                ammoList[i].gameObject.SetActive(true);
                return ammoList[i];
            }
        }

        // If no ammo is available, instantiate and add new ammo to the pool
        AmmoProjectile newAmmo = Instantiate(ammoPrefab);
        ammoList.Add(newAmmo);
        newAmmo.gameObject.SetActive(true);
        return newAmmo;
    }

    public void ReturnAmmo(AmmoProjectile ammo)
    {
        ammo.gameObject.SetActive(false);
    }
}


