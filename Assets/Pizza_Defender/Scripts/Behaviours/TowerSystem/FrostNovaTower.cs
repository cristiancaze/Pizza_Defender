using UnityEngine;

public class FrostNovaTower : TowerBase
{
    public override void OnTick()
    {
        base.OnTick();
    }

    protected override void SpecialAtack()
    {
        base.SpecialAtack();
        FrostNovaSpecialAttack();
    }

    private void FrostNovaSpecialAttack()
    {
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, stats.SpecialRangeAtack, Vector3.right, maxDistanceRaycast);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.TryGetComponent(out IFreezeable IFreezeable))
                if (targetsToAttack.Contains(IFreezeable.GetEnemieType()))
                    IFreezeable.Freeze();
        }
    }
}
