using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastWaveTower : TowerBase
{
    public override void OnTick()
    {
        base.OnTick();
    }

    protected override void SpecialAtack()
    {
        base.SpecialAtack();
        BlasWaveSpecialAttack();
    }

    private void BlasWaveSpecialAttack()
    {
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, stats.SpecialRangeAtack, Vector3.right, maxDistanceRaycast);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.TryGetComponent(out IAttackable IAttackable))
                if (targetsToAttack.Contains(IAttackable.GetEnemieType()))
                    IAttackable.TakeDamage(stats.AtackAmount);
        }
    }
}
