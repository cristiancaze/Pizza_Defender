using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostbiteTower : TowerBase
{
    public override void OnTick()
    {
        base.OnTick();

    }

    protected override void SpecialAtack()
    {
        base.SpecialAtack();
        AmmoProjectile ammoprojectile = ammoPool.GetAmmo();
        ammoprojectile.Initialize(stats.AtackAmount, true, targetsToAttack);
    }
}
