using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour, ITickable, IAttackable, IFreezeable
{
    private RaycastHit hit;
    protected int maxDistanceRaycast = 10;
    [Tooltip("What kind of enemie am I")]
    [SerializeField] private EnemyType enemyType;
    [SerializeField] protected List<EnemyType> targetsToAttack;
    [SerializeField] protected EnemyMonsterStats stats;
    protected float health;
    protected bool isActive;
    protected bool isAtacking;
    protected float currentSpeed;

    protected void Awake()
    {
        health = stats.MaxHealth;
        currentSpeed = stats.MovementSpeed;
    }

    protected virtual void Atack(IAttackable IAttackable)
    {
        IAttackable.TakeDamage(stats.AtackAmount);
        isAtacking = true;
    }

    public virtual void Initialize()
    {
        isActive = true;
    }

    protected virtual void Update()
    {
        if (!isActive)
            return;
        if (isAtacking)
            return;
        transform.position += Vector3.left * currentSpeed * Time.deltaTime;
    }

    protected virtual void SpecialAtack()
    {

    }

    public EnemyType GetEnemieType()
    {
        return enemyType;
    }

    public virtual void OnTick()
    {
        if (!isActive)
            return;
        CheckForEnemiesInRange();
    }

    public virtual void TakeDamage(float damage)
    {
        health -= damage;
        if (health < 0)
        {
            isActive = false;
            //event to return in the pool
            gameObject.SetActive(false);
        }

    }

    protected void CheckForEnemiesInRange()
    {
        if (Physics.Raycast(transform.position, Vector3.left, out hit, stats.AtackRange))
        {
            if (hit.collider.TryGetComponent(out IAttackable IAttackable))
                if (targetsToAttack.Contains(IAttackable.GetEnemieType()))
                    Atack(IAttackable);
        }
    }

    public void Freeze()
    {
        currentSpeed -= currentSpeed / 2;
    }
}
