using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class TilePlacement : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private bool isHover;
    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log(gameObject.name);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isHover = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isHover = false;
    }

    private void OnDrawGizmos()
    {
        if (isHover)
            Gizmos.DrawSphere(transform.position, .25f);
    }
}
