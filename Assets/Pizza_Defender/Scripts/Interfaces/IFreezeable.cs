using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFreezeable
{
    void Freeze();
    EnemyType GetEnemieType();
}
