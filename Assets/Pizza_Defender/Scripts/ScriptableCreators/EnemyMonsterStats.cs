using UnityEngine;

[CreateAssetMenu]
public class EnemyMonsterStats : ScriptableObject
{
    public float MovementSpeed;
    public float MaxHealth;
    [Tooltip("Interval of ticks that the attack will be executed, value 2 means 1 atack every 2 ticks")]
    public int AtacksSpeedInverval;
    [Tooltip("The maximum range effect of the basic atack")]
    public float AtackRange;
    [Tooltip("Value to deal in enemies in each atack")]
    public float AtackAmount;
    [Tooltip("Interval of ticks that the special attack will be executed, value 1 means 1 special atack every tick")]
    public int SpecialAtackInterval;
    [Tooltip("The maximum range effect of the special atack")]
    public float SpecialAtackRange;
}
