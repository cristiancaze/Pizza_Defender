using UnityEngine;

[CreateAssetMenu]
public class TowerStats : ScriptableObject
{
    public float MaxHealth;
    [Tooltip("Value to deal in enemies in each atack")]
    public float AtackAmount;
    [Tooltip("Interval of ticks that the attack will be executed, value 2 means 1 atack every 2 ticks")]
    public int AtacksSpeedInverval;
    [Tooltip("Interval of ticks that the special attack will be executed, value 1 means 1 special atack every tick")]
    public int SpecialAtackInterval;
    [Tooltip("At wich range the tower will detect enemies to start atacking")]
    public float RangeAtack;
    [Tooltip("The maximum range effect of the special atack")]
    public float SpecialRangeAtack;
    [Tooltip("Necessary resources amount to build")]
    public float BuildCost;
    [Tooltip("How many ticks will be necesary to complete the build")]
    public int TicksToBuild;
}
