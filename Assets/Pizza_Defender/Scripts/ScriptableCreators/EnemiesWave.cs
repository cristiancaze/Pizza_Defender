using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemiesWave : ScriptableObject
{
    public int EnemiesInWave;
    [Tooltip("In wich tick should the wave start")]
    public int StartWaveAtTick;
}
