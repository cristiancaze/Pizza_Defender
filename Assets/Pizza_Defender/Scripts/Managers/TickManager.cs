using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TickManager : MonoBehaviour
{
    [Tooltip("Time interval between each tick in seconds")]
    [SerializeField] private float tickInterval = 1f;
    private float timer;

    [SerializeField] private GameEvent gameEvent;

    private void Update()
    {
        timer += Time.deltaTime;

        // Check if the tick interval has passed
        if (timer >= tickInterval)
        {
            // Trigger the tick event
            gameEvent.Raise();

            timer = 0f;
        }
    }
}

