using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
    [SerializeField] private List<EnemyBase> enemyPrefabs;
    [SerializeField] private int poolSize;

    private List<EnemyBase> enemiesList;

    private void Awake()
    {
        enemiesList = new List<EnemyBase>();

        // Create the ammo pool, the ammo is the root scene for performance
        for (int i = 0; i < poolSize; i++)
        {
            EnemyBase enemy = Instantiate(enemyPrefabs[Random.Range(0,enemyPrefabs.Count)]);
            enemy.gameObject.SetActive(false);
            enemiesList.Add(enemy);
        }
    }

    public EnemyBase GetEnemy()
    {
        // Retrieve an available enemy from the pool
        for (int i = 0; i < enemiesList.Count; i++)
        {
            if (!enemiesList[i].gameObject.activeInHierarchy)
            {
                enemiesList[i].gameObject.SetActive(true);
                return enemiesList[i];
            }
        }

        // If no enemie is available, instantiate and add new enemy to the pool
        EnemyBase newEnemy = Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Count)]);
        enemiesList.Add(newEnemy);
        newEnemy.gameObject.SetActive(true);
        return newEnemy;
    }

    public void ReturnEnemy(EnemyBase enemy)
    {
        enemy.gameObject.SetActive(false);
    }
}


