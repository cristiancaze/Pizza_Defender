using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour, ITickable
{
    private EnemyPool enemyPool;
    [SerializeField] private Transform[] spawnPoints;
    [SerializeField] private List<EnemiesWave> waves;

    private int currentWave = 0;
    private int currentTick;

    private void Awake()
    {
        enemyPool = GetComponent<EnemyPool>();
    }

    
    private void Start()
    {
        StartNextWave();
    }

    public void OnTick()
    {
        currentTick += 1;
        CheckIfCanStartNewWave();
    }

    private void CheckIfCanStartNewWave()
    {
        if (currentWave >= waves.Count)
            return;

        if (currentTick == waves[currentWave].StartWaveAtTick)
            StartNextWave();
    }

    private void StartNextWave()
    {

        SpawnEnemies();
        currentWave += 1;
    }

    private void SpawnEnemies()
    {
        for (int i = 0; i < waves[currentWave].EnemiesInWave; i++)
        {
            SpawnEnemy();
        }
    }

    private void SpawnEnemy()
    {
        if (spawnPoints.Length == 0)
        {
            Debug.LogWarning("No spawn points assigned to the Enemy Manager.");
            return;
        }

        int spawnIndex = Random.Range(0, spawnPoints.Length);
        Transform spawnPoint = spawnPoints[spawnIndex];

        EnemyBase enemy = enemyPool.GetEnemy();
        enemy.transform.position = spawnPoint.position;
        enemy.transform.rotation = spawnPoint.rotation;
        enemy.Initialize();
    }

    public void ReturnEnemyToPool(EnemyBase enemy)
    {
        enemyPool.ReturnEnemy(enemy);
    }

    
}
